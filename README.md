# Description
```
A generic shell script compiler. Shc takes a script, which is specified
on the command line and produces C source code. The generated source
code is then compiled and linked to produce a stripped binary executable.
```

# Compile & Install shc
```
Compile from "main" directory:
$ make

To install:
$ sudo make install

Alternatively you can run the "install" script in terminal and it will
compile and install shc for you.
```

# Options
```
-e %s  Expiration date in dd/mm/yyyy format [none]
-m %s  Message to display upon expiration ["Message Here"]
-f %s  File name of the script to compile
-i %s  Inline option for the shell interpreter i.e: -e
-x %s  eXec command, as a printf format i.e: exec('%s',@ARGV);
-l %s  Last shell option i.e: --
-o %s  Output filename
-r     Relax security. Make a redistributable binary
-v     Verbose compilation
-D     Switch ON debug exec calls [OFF]
-U     Make binary untraceable [no]
-C     Display license and exit
-A     Display abstract and exit
-h     Display help and exit

See shc man page after installation for more in depth info:
$ man shc
```

# Usage
```
Normal use sample:
$ shc -v -r -f config.sh -o config

Expiration option sample:
$ shc -v -e 24/05/2015 -m "This file has expired" -r -f config.sh -o config

The above 2 commands will create a binary called "config" from the "config.sh"
bash script.
```

# Alternate Usage
```
If you don't wish to install the shc binary you can execute it from its current
directory as follows:
$ ./shc -v -r -f config.sh -o config
```

# Known Bug
```
The one (and I hope the only) limitation using shc is the _SC_ARG_MAX system
configuration parameter.

It limits the maximum length of the arguments to the exec function, limiting
the maximum length of the runnable script of shc.

!! - CHECK YOUR RESULTS CAREFULLY BEFORE USING - !!
```

# Resource
Forked from [neurobin](https://github.com/neurobin/shc)

Also see [credits](Credits.txt)
